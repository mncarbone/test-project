var vm = new Vue({
  el: '#app',
  data: {
    todoTxt: '',
    items: []
  },

  methods: {
    addToDo: function () {
      if(this.todoTxt != ''){
        this.items.push(this.todoTxt);
        this.todoTxt = '';
      }
    },
    removeTodo: function (index) {
      this.items.splice(index, 1);
    }
  }
});
